#!/bin/bash
yum install -y git
git clone https://gitlab.cern.ch/it-monitoring/elasticsearch-cluster-orchestration.git /etc/puppet
cd /etc/puppet
git submodule update --init --recursive
yum install -y puppet rubygem-deep_merge
# Create some facts necessary to config the node
mkdir -p /etc/facter/facts.d/
echo "hostgroup_0=masterless" > /etc/facter/facts.d/hostgroup_0.txt
echo "hostgroup_1=elasticsearch" > /etc/facter/facts.d/hostgroup_1.txt
echo "hostgroup_2=$cluster_name" > /etc/facter/facts.d/hostgroup_2.txt
echo "fename=Monitoring tools" > /etc/facter/facts.d/fename.txt
if [ "true" = "$master_node" ];
then
    echo "hostgroup=masterless/elasticsearch/$cluster_name/master" > /etc/facter/facts.d/hostgroup.txt
    echo "hostgroup_3=master" > /etc/facter/facts.d/hostgroup_3.txt
else
    echo "hostgroup=masterless/elasticsearch/$cluster_name/datanode" > /etc/facter/facts.d/hostgroup.txt
    echo "hostgroup_3=datanode" > /etc/facter/facts.d/hostgroup_3.txt
fi
# Run the show, twice...
puppet apply -tv --modulepath=/etc/puppet/modules/ /etc/puppet/manifests/
puppet apply -tv --modulepath=/etc/puppet/modules/ /etc/puppet/manifests/