node default {

  include hacks
  include lemon

  yumrepo {
    'epel':
      descr => 'Extra Packages for Enterprise Linux 7 - $basearch',
      baseurl => 'http://linuxsoft.cern.ch/epel/7/$basearch',
      enabled => 1,
      gpgcheck => 1,
      gpgkey => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7';
    'ai7-stable':
      descr => 'Utilities for the Agile Infrastructure project [stable]',
      baseurl => 'http://linuxsoft.cern.ch/internal/repos/ai7-stable/x86_64/os',
      enabled => 1,
      gpgcheck => 0,
      priority => 30;
    'itmon7-stable':
      descr => 'itmon 7 stable',
      baseurl => 'http://linuxsoft.cern.ch/internal/repos/itmon7-stable/x86_64/os',
      enabled => 1,
      gpgcheck => 0,
      priority => 99;
    'elasticsearch-1.4-stable':
      descr => 'Elasticsearch repository for 1.4.x packages',
      baseurl => 'http://linuxsoft.cern.ch/elasticsearch/elasticsearch-x86_64-stable/RPMS.es-14-el6/',
      enabled => 1,
      gpgcheck => 1,
      gpgkey => 'http://linuxsoft.cern.ch/elasticsearch/GPG-KEY-elasticsearch',
      priority => 99;
  }

  notice("Installing ${::hostgroup_3}")
  include "${::hostgroup_3}"

}
