# == Class: datanode
#
# Installs an ElasticSearch datanode
#
class datanode {

  class { 'elasticsearch':
    config => merge(hiera_hash('master_config'),hiera_hash('datanode_config')),
  }

  flume::tail { "tail-es-$::hostgroup_2":  # give a name without spaces otherwise it will fail
   files => ["/var/log/elasticsearch/$::hostgroup_2.log"],  # your list of files to tail
   out   => {
     'es1' => {
       type => 'es',
       hostNames => "$::hostgroup_2-master-0.cern.ch,$::hostgroup_2-master-1.cern.ch",  # coma separated list of ES nodes with no spaces in-between
       indexName => 'logs',  # name of the index in ES
       clusterName => $::hostgroup_2,  # name of your ES cluster
     }
   }
 }

}
