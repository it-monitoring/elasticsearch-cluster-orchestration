# == Class: hacks
#
# This class contains all the hacks necessary to work around the CERN
# integration
#
class hacks {

  include osrepos::params
  include base::params
  include rsyslog::client

  package { 'yum':
    ensure => 'present'
  }

  Osrepos::Kojitag <| title == 'itmon' |> {
    available_major_versions => []
  }

  # Create directory for dropping fact.d style facts in.
  file { $base::params::facter_path:
    ensure  => directory,
    mode    => 0644,
  } ->
  file { $base::params::factsd_path:
    ensure  => directory,
    mode    => 0644,
    purge   => true,
    recurse => true,
  } ->
  file {
    '/etc/facter/facts.d/hostgroup.txt':
      ensure => present;
    '/etc/facter/facts.d/hostgroup_0.txt':
      ensure => present;
    '/etc/facter/facts.d/hostgroup_1.txt':
      ensure => present;
    '/etc/facter/facts.d/hostgroup_2.txt':
      ensure => present;
    '/etc/facter/facts.d/hostgroup_3.txt':
      ensure => present;
    '/etc/facter/facts.d/fename.txt':
      ensure => present;
  }

}
