# ElasticSearch cluster orchestration

## Description

Doing some testing for Heat and Openstack. For the moment, you have to ask
access to a special *Heat sandbox* project (speak with Bruno Bompastor) if
you want to test it.

## Interesting links

- [Official documentation for HOT](http://docs.openstack.org/user-guide/enduser/hot-guide/hot_hello_world.html)
- [Rackspace ES Heat template](https://github.com/rackspace-orchestration-templates/elasticsearch)
- [Official Heat template examples](https://github.com/openstack/heat-templates)
- [Scaling with Heat](https://developer.rackspace.com/blog/openstack-orchestration-in-depth-part-4-scaling/)
- [User documentation for Heat](http://docs.openstack.org/user-guide/enduser/cli_create_and_manage_stacks.html)
- [Manage instance startup order in OpenStack Heat Templates](http://blog.zhaw.ch/icclab/manage-instance-startup-order-in-openstack-heat-templates/)
- [Bruno's presentation about Heat](http://indico.cern.ch/event/346931/session/9/contribution/22/material/slides/0.pptx
)

## ElasticSearch

### How to create a new ES cluster?

Using the command line interface that `Heat` provides and using the template
inside the `elasticsearch/` folder of this repository.

The template provides the following parameters:

- **cluster_name** (Mandatory): The name of the ES cluster. It will be used to
generate the name of the machines.
- **es_master_nodes** (Default: *2*): The number of master nodes. *Broken: Leave
the default number*
- **es_master_flavor** (Default: *m1.medium*): The flavor of the master nodes of
the ES cluster.
- **es_data_nodes** (Default: *2*): The number of data nodes.
- **es_data_flavor** (Default: *m1.medium*): The flavor of the data nodes of
the ES cluster.
- **image_name** (Default: *CC7 Extra - x86_64 [2015-02-10]*): The image name
that is going to be used for all the machines.

For example, this command:

```bash
$ heat stack-create mystack \
    --template-file elasticsearch-heat.yaml \
    --parameters "cluster_name=itmon-testing;es_data_nodes=2;es_master_nodes=2"
```

will create the following a stack with 2 master and data nodes with the
`m1.medium` flavor. The name of the machines will be
`cluster_name` + `type` + `index`. For example, for our command:

- itmon-testing-master-0
- itmon-testing-master-1
- itmon-testing-data-0
- itmon-testing-data-1

After was initialized, you should be able to see all the nodes were included in
the cluster with:

```bash
$ curl itmon-testing-master-0.cern.ch:9200/_cat/nodes
itmon-testing-master-1 XXX.YYY.ZZZ.AAA 5 13 0.00 - * es-master-1
itmon-testing-data-1   XXX.YYY.ZZZ.AAA 3 16 0.00 d - es-data-1
itmon-testing-data-0   XXX.YYY.ZZZ.AAA 3 17 0.00 d - es-data-0
itmon-testing-master-0 XXX.YYY.ZZZ.AAA 6 13 0.00 - m es-master-0
```

And access to Kibana 4, pointing to the port 5601 in one of master nodes (in
this case http://itmon-testing-master-0.cern.ch:5601)

This information can be checked in any moment for any stack using the heat
commands `stack-show`, `output-list` and `output-show`. The available outputs
for the user after creating the stack are:

| output_key | description                                                                   |
|------------|-------------------------------------------------------------------------------|
| data_nodes | The name of the created data nodes                                            |
| es_url     | The ES URL. Do `curl http:///<url>/_cat/nodes` to see all the nodes of the cluster. |
| kibana_url | The kibana URL. Access it through a web browser                               |
| masters    | The name of the created masters                                               |


For example:

```bash
$ heat output-show itmon-testing es_url
"http://itmon-testing-master-0.cern.ch:9200"
$ heat output-show aaaac masters
[
  "itmon-testing-master-0",
  "itmon-testing-master-1"
]
```

---

If you want to use the web interface in order to create a stack, you
can go to `Orchestration` -> `Launch stack` and then upload
`elasticsearch-heat.yaml`. A new webform will show up with all the parameters
that you can define for the template. At the moment, this is not working
because I defined an external script (`es_init.sh`) that cannot be uploaded
to the web interface.

More information and future cool features that will be probably added to
Horizon in [this video](https://fedorapeople.org/~radez/thermal20121205.ogv).


### How to add more data nodes to my ES cluster?

Using `heat stack-update` you should be able to update settings for your stack.
If you want to add more data nodes to your stack, you have to specify the
new number as a parameter of the command line. Working with our previous created
stack, we have 2 data nodes and we want to have more nodes because we are
running out of disk space (imagine that we want to have 4 data nodes).

```bash
$ heat stack-update mystack \
    --template-file elasticsearch-heat.yaml \
    --parameters "cluster_name=itmon-testing;es_data_nodes=4"
```

Heat will detect that in our stack two more nodes are needed, and they will
be created with the same settings and they will be added to the existing
ES cluster.

It also works the other way around, if the ammount of data nodes specified
is less than the existing number of nodes, Heat will destroy randomly nodes
until the number of data nodes is correct.

Resizing the flavor of the machine should be also possible (but it was
failing when I tried).

## Structure

```
./clones/                   Git submodules for required Puppet modules
./hieradata/                Hiera data tree
./hieradata/module_names/   Symlinks to modules data
./hieradata/common.yaml     General purpose hiera file
./manifests/
./manifests/site.pp         Base site.pp
./manifests/hacks.pp        Compilation of hacks to workaround Cern
./modules/                  Symlinks to modules code
./hiera.yaml                Hiera config file
./puppet.conf               Puppet config file
./templates/                Folder for configuration templates
```

### How to run Puppet

1. Clone this repository into `/etc/puppet`
```
git clone ...
```
2. Initialize submodule
```
git submodule update --init --recursive
```
3. Run puppet apply
```
puppet apply -tv --modulepath=/etc/puppet/modules/ /etc/puppet/manifests/
```

### Things to improve before deploying in production

- Use `puppet apply` and configuration file options will be a better choice when
they can be used than a simple script to define stuff.
- Fix the master nodes option in the parameters.
- The nodes generated are not configured to run puppet at the moment.
- Some autoscaling or load balancing options can be taken into account.
